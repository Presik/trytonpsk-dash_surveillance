# This file is part of purchase_discount module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.transaction import Transaction
from trytond.model import ModelView, ModelSQL, fields


class AppSurveillanceSchedule(ModelSQL, ModelView):
    'App Surveillance Schedule'
    __name__ = 'dash.app.surveillance_schedule'
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or None
